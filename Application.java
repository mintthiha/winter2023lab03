public class Application {
	public static void main(String[] args){
		Student jenny = new Student();
		jenny.height = 162.3;
		jenny.age = 17;
		jenny.favColor = "Green";
		
		Student haris = new Student();
		haris.height = 194.7;
		haris.age = 19;
		haris.favColor = "Black";
		
		System.out.println(jenny.height);
		System.out.println(jenny.age);
		System.out.println(jenny.favColor);
		System.out.println();
		System.out.println(haris.height);
		System.out.println(haris.age);
		System.out.println(haris.favColor);
		System.out.println();
		
		jenny.sayHeight(jenny.height);
		haris.sayHeight(haris.height);
		System.out.println();
		
		Student[] section4 = new Student[3];
		section4[0] = jenny;
		section4[1] = haris;
		section4[2] = new Student();
		section4[2].height = 178.5;
		section4[2].age = 21;
		section4[2].favColor = "Blue";
		
		System.out.println(section4[2].height);
		System.out.println(section4[2].age);
		System.out.println(section4[2].favColor);
	}
}
	
	